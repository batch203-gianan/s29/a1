
// 2 ok

db.users.find(
	{	 
		$or: [
			 {firstName: 
			 	{$regex: 's', $options :'$i'}
			 },

			 {lastName: 
			 	{$regex: 'd', $options :'$i'}
			 },
		]
	},
	{_id:0}

);

//3 ok

db.users.find(
		{
			$and:[
				{age: {$gte: 70}},
				{department: "HR"}
			]
		}

);

//4 ok

db.users.find(
	{	 
		$and: [
			 {firstName: 
			 	{$regex: 'e', $options :'$i'}
			 },

			 { age: {$lte: 30} },
		]
	}
);
